var page;

// first thing we do - xsrf token
fetch('/django/fantasyPLAPI/', {
  method: 'GET',
  credentials: 'include',
  mode: 'cors'
});

/* 
Name: searchJSONArray
Input: Takes an array of JSON objects, and a key-value pair as inputs
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function searchJSONArray(array, key, value) {
  for (element in array) {
    if (array[element][key] == value) {
      return element;
    }
  };
  return -1;
};

/* 
Name: mergeJSONObjects
Description: merges two JSON objects together, joining on a shared key
Input: 
	baseObject - object to receive merged data
	baseObjectField - object field to join on
	mergeObject - object to merge with baseObject
	mergeObjectField - object field to join on (to match baseObjectField)
	fieldsToMerge - array of fields to merge into baseObject
	searchChildren - boolean. Search through child objects?
Output: Returns the merged version of baseObject
13/04/2019
*/
function mergeJSONObjects(baseObject, baseObjectField, mergeObject, mergeObjectField, fieldsToMerge, searchChildren) {
  let returnObject = baseObject;

  if (returnObject[baseObjectField] == mergeObject[mergeObjectField]) {

    for (let i = 0; i < fieldsToMerge.length; i++) {
      returnObject[fieldsToMerge[i]] = mergeObject[fieldsToMerge[i]];
    };
  };

  if (searchChildren && returnObject.children) {
    for (let j = 0; j < returnObject.children.length; j++) {
      returnObject.children[j] = mergeJSONObjects(returnObject.children[j], baseObjectField, mergeObject, mergeObjectField, fieldsToMerge, searchChildren);
    };
  };

  return returnObject;
};

/* 
Name: trimJSONObject
Description: "trims" a hierarchical JSON object, removing 
	any empty nodes
Input: 
	inputObject - object to trim
	inputObjectField - field to test emptiness
	searchChildren - boolean. Search through child objects?
Output: 
	Returns the trimmed version of inputObject
14/04/2019
*/
function trimJSONObject(inputObject, inputObjectField, searchChildren, reduceStructure) {
  let returnObject = inputObject;

  // enter children first
  if (returnObject.children) {
    // trim all the element's children
    for (let i = 0; i < returnObject.children.length; i++) {
      returnObject.children[i] = trimJSONObject(returnObject.children[i], inputObjectField, searchChildren, reduceStructure)
    };

    // eradicate any undefined (trimmed) elements
    returnObject.children = returnObject.children.filter(
      function (el) {
        return el != null;
      }
    );
  };

  let numberOfChildren = returnObject.children ? returnObject.children.length : 0;
  if (numberOfChildren == 1 && (returnObject[inputObjectField] == undefined || returnObject[inputObjectField] == "" || returnObject[inputObjectField] == "placeholder") && reduceStructure) {
    returnObject = returnObject.children[0];
  };

  // return the object if either:
  // a. it contains the field we're looking for
  // b. it has children (who will only exist if they contain the field we're looking for)
  if ((returnObject[inputObjectField] && returnObject[inputObjectField] != "placeholder" && returnObject[inputObjectField] != "derived") || numberOfChildren > 0) {
    return returnObject;
  };

};

/* 
Name: getJSONDepth
Description: Returns the depth of a json object
Input: 
	obj - object to return depth for
	childField - name of field containing children
Output: 
	Returns the depth of inputObject
22/05/2019
*/
function getJSONDepth(obj, childField) {
  var depth = 0;
  if (obj[childField]) {
    obj[childField].forEach(function (d) {
      var tmpDepth = getJSONDepth(d, childField)
      if (tmpDepth > depth) {
        depth = tmpDepth
      }
    })
  }
  return 1 + depth
};


/* 
Name: flattenJSON
Description: "Flattens" a hierarchical JSON object,
	returning an array of all the object's nodes
Input: 
	obj - object to return depth for
	childField - name of field containing children
Output: 
	Returns an array of all the object's nodes
26/05/2019
*/
function flattenJSON(obj, childField) {
  returnArray = [];

  // deep copy so that original obj is not affected
  let objToPush = jQuery.extend(true, {}, obj);
  delete objToPush[childField];
  returnArray.push(objToPush);

  // if object has children, recurse
  if (obj[childField]) {
    for (let i = 0; i < obj[childField].length; i++) {
      returnArray = returnArray.concat(flattenJSON(obj[childField][i], childField));
    }
  };

  return returnArray;
};


function trimWS(inputString) {
  return inputString.replace(/\s/g, "").replace(/,/g, "").toLowerCase();
};
