/* 
Name: buildInput
Description: Builds a html input
Inputs: 
	attributes: an array of the html attributes to be used
	label: the input's label
Output: none
07/09/2018 
*/
function buildInput(attributes, label) {
	var attributeString="";
	for (attribute in Object.keys(attributes)) {
		var key = Object.keys(attributes)[attribute];
		var value = attributes[key];
		attributeString += ' ' + key + '="' + value + '"';
	}
	var inputID = "form-" + trimWS(label)
	$("#addOptionForm").append('<div id="' + inputID + '">');
	var inputDiv = $('[id="' + inputID + '"]');
	inputDiv.append('<input ' + attributeString + '>');
	if (label != undefined) {
		inputDiv.append('<label>' + label + '</label>');
	};
	$("#addOptionForm").append('</div>');
};

/* 
Name: buildSlider
Description: Builds a html slider
Inputs: 
	attributes: an array of the html attributes to be used
	label: the input's label
	showValue (optional): whether or not to show the value
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildSlider(attributes, label, showValue) {

	var attributeString="", sliderName;
	for (attribute in Object.keys(attributes)) {
		var key = Object.keys(attributes)[attribute];
		var value = attributes[key];
		if (key == "name") {sliderName = attributes[key]};
		attributeString += ' ' + key + '="' + value + '"';
	}
	var inputID = "form-" + trimWS(label)
	$("#addOptionForm").append('<div id="' + inputID + '">');
	var inputDiv = $('[id="' + inputID + '"]');

	if (showValue) {
		const changeFunction = '$("#output-' + sliderName + '").html(this.value)';
		inputDiv.append('<input onchange=' + changeFunction + ' ' + attributeString + '>');
		if (label != undefined) {
			inputDiv.append('<label>' + label + ':</label>');
		};
		inputDiv.append('<span id="output-' + sliderName + '">' + showValue + '</span>');
	} else {
		
	};
	// if (showValue == 1) {
		// inputDiv.append('<span id="output-' + sliderName + '">');		
	// };
	$("#addOptionForm").append('</div>');	
	
}


/* 
Name: buildRadioInput
Description: Builds a radio group
Inputs: 
	name: the name of the radio group
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildRadioInput(name, options) {
	$("#addOptionForm").append('<div class="radioGroup">');
	var id;
	for (let i=0; i<options.length; i++) {
		var attributeString="";
		for (attribute in Object.keys(options[i]["attributes"])) {
			var key = Object.keys(options[i]["attributes"])[attribute];
			var value = options[i]["attributes"][key];
			if (key=="id") {id=value;};
			attributeString += ' ' + key + '="' + value + '"';
		}		
		$(".radioGroup").append('<input name="' + name + '" type="radio"' + attributeString + '>');
		$(".radioGroup").append('<label for="' + id + '">' + options[i]["name"] + '</label>');
	};
};

/* 
Name: buildTextInput
Description: Builds a textbox input
Inputs: 
	id: the ID of the html element
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildButton(onclick, buttonText, id) {
	var idString="";
	if (id) {idString = 'id="' + id + '"'};
	$("#addOptionForm").append('<button ' + idString + ' onclick="' + onclick + '">' + buttonText + '</button>');
};

/* 
Name: buildTextInput
Description: Builds a textbox input
Inputs: 
	id: the ID of the html element
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildDropdownList(id, options, label, parentID, width) {
	if (parentID == undefined) {
		parentID = "addOptionForm"
		$("#" + parentID).append('<ul class="custom-dropdown" id="' + id + '"></ul>');
	};
	var idString = "";
	if (options[0].id) {idString = " id='" + options[0].id + "'";};
	$("#" + parentID).append('<div class="column" style="width:' + width + ';"><ul class="custom-dropdown" id="' + id + '"></ul></div>');
	$("#" + id).before("<span>" + label + "</span>");
	$("#" + id + "").append("<li" + idString + " class='selected' onclick='$(&quot;#" + id + " .selected&quot;).removeClass(&quot;selected&quot;); $(this).addClass(&quot;selected&quot;);'>" + options[0].name + "</li>");			
	for (let i = 1; i < options.length; i++) {
		if (options[i].id) {idString = " id='" + options[i].id + "'";};
		$("#" + id + "").append("<li" + idString + " onclick='$(&quot;#" + id + " .selected&quot;).removeClass(&quot;selected&quot;); $(this).addClass(&quot;selected&quot;);'>" + options[i].name + "</li>");			
	};	
};


/* 
Name: buildTextInput
Description: Builds a textbox input
Inputs: 
	id: the ID of the html element
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildMultiDropdownList(id, options, label, parentID, width) {
	if (parentID == undefined) {
		parentID = "addOptionForm"
		$("#" + parentID).append('<ul class="custom-dropdown" id="' + id + '"></ul>');
	};
	$("#" + parentID).append('<div class="column" style="width:' + width + ';"><ul class="custom-dropdown" id="' + id + '"></ul></div>');
	$("#" + id).before("<span>" + label + "</span>");
	for (let i=0; i<options.length; i++) {
		var attributeString="";
		for (attribute in Object.keys(options[i]["attributes"])) {
			var key = Object.keys(options[i]["attributes"])[attribute];
			var value = options[i]["attributes"][key];
			// if (key=="id") {id=value;};
			attributeString += ' ' + key + '="' + value + '"';
		}
		$("#" + id + "").append("<li" + attributeString + ">" + options[i].name + "</li>");			
	};
};

/* 
Name: buildTextInput
Description: Builds a textbox input
Inputs: 
	id: the ID of the html element
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildSelectList(id, options, label, parentID, width) {
	if (parentID == undefined) {
		parentID = "addOptionForm"
		$("#" + parentID).append('<ul class="custom-select" id="' + id + '"></ul>');
	};
	$("#" + parentID).append('<div class="column" style="width:' + width + '; margin: auto;"><ul class="custom-select" id="' + id + '"></ul></div>');
	$("#" + id).before("<span>" + label + "</span>");
	for (let i=0; i<options.length; i++) {
		var attributeString="";
		for (attribute in Object.keys(options[i]["attributes"])) {
			var key = Object.keys(options[i]["attributes"])[attribute];
			var value = options[i]["attributes"][key];
			// if (key=="id") {id=value;};
			attributeString += ' ' + key + '="' + value + '"';
		}
		$("#" + id + "").append("<li" + attributeString + ">" + options[i].name + "</li>");			
	};
};


/* 
Name: buildTextInput
Description: Builds a textbox input
Inputs: 
	id: the ID of the html element
Output: Returns the position in the array of the first JSON object that contains the key-value pair
07/09/2018 
*/
function buildMultiSelectList(id, options, label, parentID, width) {
	if (parentID == undefined) {
		parentID = "addOptionForm"
		$("#" + parentID).append('<ul class="custom-select" id="' + id + '"></ul>');
	};
	$("#" + parentID).append('<div class="column" style="width:' + width + ';"><ul class="custom-select" id="' + id + '"></ul></div>');
	$("#" + id).before("<span>" + label + "</span>");
	for (let i=0; i<options.length; i++) {
		var attributeString="";
		for (attribute in Object.keys(options[i]["attributes"])) {
			var key = Object.keys(options[i]["attributes"])[attribute];
			var value = options[i]["attributes"][key];
			// if (key=="id") {id=value;};
			attributeString += ' ' + key + '="' + value + '"';
		}
		$("#" + id + "").append("<li" + attributeString + ">" + options[i].name + "</li>");			
	};
};




