var languageTree, rootList, selectedSoundChange
var originalWord
var languages, allLanguages, languageGroups
var rootLang = 'ine-pro'; var currentRoot; var currentSoundChange; var currentCategory
var test
const borrowedColour = '#EA9C6A';
const primaryColour = 	'#66b3ff';
const soundChangeColour = '#dcb400';
const directDescendantColour = '#cccccc';
const greyOutlineColour = '#33597f';
const margin = { left: 12, top: 20, right: 0, bottom: 20 }
// var i=0;
var duration = 750
const originalHeight = $('.graph-options-container').height()
var childToMove = {}

// read cookies and do things accordingly
const showRootCookieName = 'showRoots';
const adminCookieName = 'isInAdminMode';
const ipaCookieName = 'showIPA';
const adminCodeCookieName = 'adminCode';
const csrfCookieName = 'csrftoken';
var csrfValue;
function readCookiesAndAct () {
  var cookieString = document.cookie

    //get the csrf token
    var csrfIndex = cookieString.indexOf(csrfCookieName)
    if (csrfIndex != -1) {
    var csrfValueIndex = csrfIndex + csrfCookieName.length + 1
        var csrfEndIndex = cookieString.indexOf(';', csrfValueIndex)
        var csrfValueLength = csrfEndIndex - csrfValueIndex
        csrfValue = cookieString.substr(csrfValueIndex, csrfValueLength)
    };


    // are we showing roots?
    var showRootIndex = cookieString.indexOf(showRootCookieName)
    if (showRootIndex != -1) {
    var showRootValueIndex = showRootIndex + showRootCookieName.length + 1
        var showRootEndIndex = cookieString.indexOf(';', showRootValueIndex)
        var showRootValueLength = showRootEndIndex - showRootValueIndex
        var showRootValue = cookieString.substr(showRootValueIndex, showRootValueLength)
  };

  // are we in admin mode?
  var adminModeIndex = cookieString.indexOf(adminCookieName)
    if (adminModeIndex != -1) {
    var adminModeValueIndex = adminModeIndex + adminCookieName.length + 1
        var adminModeEndIndex = cookieString.indexOf(';', adminModeValueIndex)
        var adminModeValueLength = adminModeEndIndex - adminModeValueIndex
        var adminModeValue = cookieString.substr(adminModeValueIndex, adminModeValueLength)
  };

  // are we showing IPA transcriptions?
  var ipaModeIndex = cookieString.indexOf(ipaCookieName)
    if (ipaModeIndex != -1) {
    var ipaModeValueIndex = ipaModeIndex + ipaCookieName.length + 1
        var ipaModeEndIndex = cookieString.indexOf(';', ipaModeValueIndex)
        var ipaModeValueLength = ipaModeEndIndex - ipaModeValueIndex
        var ipaModeValue = cookieString.substr(ipaModeValueIndex, ipaModeValueLength)
    };

  // has the admin code been set?
  var adminCodeIndex = cookieString.indexOf(adminCodeCookieName)
    if (adminCodeIndex != -1) {
    var adminCodeValueIndex = adminCodeIndex + adminCodeCookieName.length + 1
        var adminCodeEndIndex = cookieString.indexOf(';', adminCodeValueIndex)
        var adminCodeValueLength = adminCodeEndIndex - adminCodeValueIndex
        var adminCodeValue = cookieString.substr(adminCodeValueIndex, adminCodeValueLength)
    };
};
readCookiesAndAct()

async function getAllCategories () {
  const result = await fetch('/django/indo-european-cognates-2/',
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: { 'requestType': 'getAllLanguages' },
      credentials: 'include',
      mode: 'cors'
    }
  );
    return result

}

// get all languages, regardless of whether or not we have words for them
async function getAllLanguages () {
  const result = await fetch('/django/indo-european-cognates-2/',
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: { 'requestType': 'getAllLanguages' },
      credentials: 'include',
      mode: 'cors'
    }
  )
    return result
};

async function getAllDescendantsOfRoot () {
  const result = await fetch('/django/indo-european-cognates-2/'
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'getAllChildLangs',
        'rootLang': root
      },
      credentials: 'include',
      mode: 'cors'
    }
  )		
    console.log(result)
    return result
};

// get all languages that exist as roots of trees
async function getAllRootLangs () {
  const result = await fetch('/django/indo-european-cognates-2/',
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: { 'requestType': 'getAllRootLangs' },
      credentials: 'include',
      mode: 'cors'
    }
  )		

    return result
};

// remove a word from the db (i.e. this word is NOT a descendant of the root)
function deleteWord (word) {
  var wordToDelete = {}
    wordToDelete.parentLatinTranscription = word.parentLatinTranscription
    wordToDelete.latinTranscription = word.latinTranscription
    wordToDelete.parentLang = word.parentLang
    wordToDelete.lang = word.lang
    wordToDelete.rootLatinTranscription = word.rootLatinTranscription
    
    fetch('/django/indo-european-cognates-2/',
    { 	method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'deleteWord',
        'adminCode': $('input[name=adminCode]').val(),
        'wordToDelete': JSON.stringify(wordToDelete)
      },
      credentials: 'include',
      mode: 'cors'
    })
};

// we're editing a word - submit the changes
// function submitWordChanges () {
//   $('#af-languageName').prop('disabled', false)
//     var tags = []; var soundChanges = []
//     var soundChangeItems = $('#soundChange-list > li')
//     for (let i = 1; i <= soundChangeItems.length; i++) {
//     soundChanges.push({ 'name': $('#soundChange-list > li:nth-child(' + i + ')').attr('data-value'), 'val': $('#soundChange-list > li:nth-child(' + i + ')').hasClass('selected') })
//   };
//   var tagItems = $('#tag-dropdown > li')
//     for (let i = 1; i <= tagItems.length; i++) {
//     tags.push({ 'name': $('#tag-dropdown > li:nth-child(' + i + ')').attr('data-value'), 'val': $('#tag-dropdown > li:nth-child(' + i + ')').hasClass('selected') })
//   };

//   var wordToSubmit = {
//     'lang': $('#af-languageCode').val(),
//     'latinTranscription': $('#af-latinTranscription').val(),
//     'rootLatinTranscription': $('#af-rootLatinTranscription').val(),
//     'rootLang': $('#af-rootLanguageCode').val(),
//     'ipaTranscription': $('#af-ipaTranscription').val(),
//     'meaning': $('#af-meaning').val(),
//     'parentLang': $('#af-parentLanguageCode').val(),
//     'parentLatinTranscription': $('#af-parentLatinTranscription').val(),
//     'isBorrowed': $('#af-isBorrowed input').prop('checked'),
//     'tags': tags,
//     'soundChanges': soundChanges
//   }
//     $.ajax(
//     { 	method: 'POST',
//       headers: { 'x-csrftoken': csrfValue },
//       body: {
//         'requestType': 'submitWord',
//         'adminCode': $('input[name=adminCode]').val(),
//         'originalWord': JSON.stringify(originalWord),
//         'wordToSubmit': JSON.stringify(wordToSubmit)
//       },
//       url: '/django/indo-european-cognates-2/',

//       credentials: 'include',
//       mode: 'cors',
//       success: function (data) {
//         if (!data.success) {
//           alert('Incorrect code, please enter again.')
//                 toggleSettingsForm()
//                 if (currentMode == 'edit' || currentMode == 'add') {
//             toggleAdminForm()
//                 }
//         } else {
//           if (currentMode == 'edit' || currentMode == 'add') {
//             toggleAdminForm()
//                 }
//           drawChart(currentRoot)
//                 alert('Success!')
//             };
//       }
//     }
//   )		
//     $('i[data-active=1]').attr('data-active', 0)
//     currentMode = 'default';
// };

// given a list of root languages, append them
// to the appropriate <select> elemen
function buildRootLangList (rootLangs) {
  rootLangRow = d3.select('#searchByRootLang')
    rootLangRow.selectAll('.none')
    .data(rootLangs)
    .enter()
    .append('option')
    .attr('value', function (d) { return d.code })
    .text(function (d) { return d.name })
                	

};

// our root langs are originally returned as their TLCs.
// Here we convert those TLCs into friendly names
function addNamesToRootLangs (rootLangs, langs) {
  return (rootLangs.map(e1 => {
    e1 = langs.filter(e2 =>	e2.code == e1)[0]
        return (e1)
    }))
};

// get all root words for a given root lang, and populate lists
async function getAllRootWords (root) {
    const result = await fetch('/django/indo-european-cognates-2/',
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'getDistinctRoots',
        'rootLang': root,
        'returnRoots': returnRoots
      },
      credentials: 'include',
      mode: 'cors'
    }
  )
    let rootList = result.results
    rootLang = root
    let wordDropdownArray = []
    for (let i = 0; i < rootList.length; i++) {
    wordDropdownArray.push({ 'name': rootList[i].word + ' (' + rootList[i].rootLatinTranscription + ')', 'rootLatinTranscription': rootList[i].rootLatinTranscription, 'tags': rootList[i].tags })
    };
  var dropdownElement = d3.selectAll('#word-dropdown')
    
    dropdownElement.selectAll('.none')
    .data(wordDropdownArray)
    .enter()
    .append('li')
    .on('click', drawChart)
    .text(function (d) { return d.name })
        

    var selectElement = d3.selectAll('#wordSelect, #soundchange-word-dropdown')


    selectElement.selectAll('.none')
    .data(rootList)
    .enter()
    .append('option')
    .attr('value', function (d) {
      return d.rootLatinTranscription
        })
    .text(function (d) { return d.rootLatinTranscription + ' (' + d.word + ')' })
        
    
    return result
};

// get list of language groupings (to make it prettier)
async function getLanguageGroups () {
  const result = await fetch('indoEuropeanGroups.json',{
    type: 'GET',
    credentials: 'include'
  })
  return result
};

// return all words in a given language
async function getAllWordsInLang (langCode) {
  const result = await $.ajax('/django/indo-european-cognates-2/',{
    method: 'POST',
    headers: { 'x-csrftoken': csrfValue },
    body: {
      'requestType': 'getAllWordsInLanguage',
      'lang': langCode
    },
    credentials: 'include',
    mode: 'cors'
  })

    return result

};

async function makeLanguageGroups() {

    rootLanguagePromise = getAllRootLangs() // get all the root langs so we can populate the dropdown array
    allLanguages = await getAllLanguages();
    var rootWordPromise = getAllWordsInLang(rootLang);
    allLanguages = allLanguages.results;
    rootLanguages = await rootLanguagePromise;
    
    rootLanguages = rootLanguages.results;

    rootLanguages = addNamesToRootLangs(rootLanguages, allLanguages);
    buildLanguageAutocomplete(allLanguages);

    buildRootLangList(rootLanguages);
    $("#searchByRootLang").val("ine-pro");
    $("#searchLanguages").val("Proto-Indo-European");
    languageGroups = await getLanguageGroups();
    var rootWords = await rootWordPromise;
    rootWords = rootWords.results;

    buildFreeTextWordAutocomplete(rootWords);
    // get names of proto langs
    
};

async function buildFreeTextWordAutocomplete (words) {
  // clear value
  $('#searchWords').val('')
    
                // set up autocomplete
                let autoCompleteArray = words.map(e => { return { 'label': e.latinTranscription + ' (' + e.meaning + ')', 'value': e.rootLatinTranscription, 'rootLang': e.rootLang } })
                $('#searchWords').autocomplete({
    source: autoCompleteArray,
    minLength: 2,
    select: function (event, ui) {
      rootLang = ui.item.rootLang
                        $('#searchByRootLang').val(rootLang)
                        drawChart(ui.item.value)
                    }
  })
};

// set up the different language autocomplete lists
async function buildLanguageAutocomplete (languages) {
  // set up autocomplete
  let autoCompleteArray = languages.map(e => { return { 'label': e.name, 'value': e.name } })

    $('#af-parentLanguageName').autocomplete({
    source: autoCompleteArray,
    minLength: 3,
    select: function (event, ui) {
      var selectedLanguageCode = languages.filter(e => e.name == ui.item.value)[0].code
            $('#af-parentLanguageCode').val(selectedLanguageCode)
    }
  })

    $('#af-languageName').autocomplete({
    source: autoCompleteArray,
    minLength: 3,
    select: function (event, ui) {
      var selectedLanguageCode = languages.filter(e => e.name == ui.item.value)[0].code
                $('#af-languageCode').val(selectedLanguageCode)
    }
  })
            
    $('#searchLanguages').autocomplete({
    source: autoCompleteArray,
    appendTo: '#searchLanguagesContainer',
    minLength: 3,
    select: function (event, ui) {
      var selectedLanguageCode = languages.filter(e => e.name == ui.item.value)[0].code
                $('#searchLanguages').val(ui.item.value)
                var words
                var wordPromise = getAllWordsInLang(selectedLanguageCode)
                wordPromise.then(function (words) {
        buildFreeTextWordAutocomplete(words.results)
                })
    }
  })
};

makeLanguageGroups()

// count the number of trees that contain each sound change
function countSoundChanges () {
  var returnRoots = $('#rootToggleContainer input').prop('checked')
    $.ajax(
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'countSoundChanges',
        'rootLang': rootLang,
        'returnRoots': returnRoots
      },
      url: '/django/indo-european-cognates-2/',
      credentials: 'include',
      mode: 'cors',
      success: function (data) {
        // set sound changes tags to (0)
        // we have to do this because the AJAX call
        // returns nothing for some sound changes.
        var soundChangeSelect = $('#soundChangeList option')
                for (let i = 1; i < soundChangeSelect.length; i++) {
          let optionItem = $('#soundChangeList option:nth-child(' + (i + 1) + ')')
          var optionString = optionItem.text()
                    var bracketIndex = optionString.indexOf('(')
                    var rootString = optionString.substr(0, bracketIndex - 1)
          $('#soundChangeList option:nth-child(' + (i + 1) + ')').text(rootString + ' (0)')
        };

        for (soundChange in data.results) {
          // <option> elements can only contain text, so we need to reassemble the entire string.
          var optionString = $('option[data-change=' + soundChange + ']').text()
                    var bracketIndex = optionString.indexOf('(')
                    var rootString = optionString.substr(0, bracketIndex - 1)
          $('option[data-change=' + soundChange + ']').text(rootString + ' (' + data.results[soundChange] + ')')
                };
      }
    })
};
countSoundChanges()

// count the number of trees that are tagged with each value
function countTags () {
  var returnRoots = $('#rootToggleContainer input').prop('checked')
    $.ajax(
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'countTags',
        'rootLang': rootLang,
        'returnRoots': returnRoots
      },
      url: '/django/indo-european-cognates-2/',
      credentials: 'include',
      mode: 'cors',
      success: function (data) {
        // set all tags to (0)
        var tagSelect = $('#categorySelect option')
                for (let i = 1; i < tagSelect.length; i++) {
          let optionItem = $('#categorySelect option:nth-child(' + (i + 1) + ')')
          var optionString = optionItem.text()
                    var bracketIndex = optionString.indexOf('(')
                    var rootString = optionString.substr(0, bracketIndex - 1)
          $('#categorySelect option:nth-child(' + (i + 1) + ')').text(rootString + ' (0)')
        };

        for (tag in data.results) {
          // <option> elements can only contain text, so we need to reassemble the entire string.
          var optionString = $('option[data-tag=' + tag + ']').text()
                    var bracketIndex = optionString.indexOf('(')
                    var rootString = optionString.substr(0, bracketIndex - 1)
          $('option[data-tag=' + tag + ']').text(rootString + ' (' + data.results[tag] + ')')
                };
      }
    })
};
countTags()

// return all of the words in a given language
// this is mainly for populating the autocomplete search
function returnAllWordsInLanguage (d) {
  $('#word-dropdown').empty()
    $(this).parent().children().removeClass('selected')
    $(this).addClass('selected')
    $.ajax(
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'getAllWordsInLanguage',
        'lang': d.code
      },
      url: '/django/indo-european-cognates-2/',
      credentials: 'include',
      mode: 'cors',
      success: function (data) {
        rootList = data
                console.log(rootList)
                
                // set up autocomplete
                autoCompleteArray = rootList.results.map(e => { return { 'label': e.latinTranscription + ' (' + e.meaning + ')', 'value': e.latinTranscription } })
                
                $('#searchWords').autocomplete({
          source: autoCompleteArray,
          minLength: 1,
          appendTo: '#searchContainer',
          select: function (event, ui) {
            let inputObj = { 'name': ui.item.value, 'pieWord': ui.item.value }
            drawChart(inputObj)
                    }
        })
            }
    }
  )
}

// rootLang starts as "ine-pro", the default
returnAllWordsInLanguage({ 'code': rootLang })
getAllRootWords(rootLang)

// show/hide inputs on changing tab
$('#selectTab').on('click', function () {
  getAllRootWords(rootLang)
    $('#selectContainer').show()
    $('#searchContainer').hide()
    $('#soundChangeContainer').hide()
    $('#searchByTab > *').addClass('inactiveTab')
    $('#searchByTab > *').removeClass('activeTab')
    $(this).addClass('activeTab')
    $(this).removeClass('inactiveTab')
})

$('#searchTab').on('click', function () {
  $('#selectContainer').hide()
    $('#searchContainer').show()
    $('#soundChangeContainer').hide()
    $('#searchByTab > *').addClass('inactiveTab')
    $('#searchByTab > *').removeClass('activeTab')
    $(this).addClass('activeTab')
    $(this).removeClass('inactiveTab')
})

$('#soundChangeTab').on('click', function () {
  $('#selectContainer').hide()
    $('#searchContainer').hide()
    $('#soundChangeContainer').show()
    $('#searchByTab > *').addClass('inactiveTab')
    $('#searchByTab > *').removeClass('activeTab')
    $(this).addClass('activeTab')
    $(this).removeClass('inactiveTab')
})

// get etymology tree for a given word.
function drawChart (rootLatinTranscription, selectedSoundChange) {
  currentRoot = rootLatinTranscription
    if (currentRoot == undefined) { return};
  $(this).siblings().removeClass('selected')
    $(this).addClass('selected')
    // For feedback: highlight the option just selected
    $('#wordDropdown li').removeClass('selected')
    // clear the canvas before we redraw
    $('#svgCanvas > svg > g').empty()
    $('.graph-headword').remove()
    $.ajax(
    {
      method: 'POST',
      headers: { 'x-csrftoken': csrfValue },
      body: {
        'requestType': 'getWord',
        'word': rootLatinTranscription
      },
      url: '/django/indo-european-cognates-2/',
      credentials: 'include',
      mode: 'cors',
      success: function (data) {
        console.log(data)
                for (let i = 0; i < allLanguages.length; i++) {
          for (let j = 0; j < data.results.length; j++) {
            if (allLanguages[i].code == data.results[j].lang || (allLanguages[i].code + '-Latn') == data.results[j].lang) {
              data.results[j].languageName = allLanguages[i].name
                        }
          };
        };
        // we use jquery.extend to deep copy the object
        let cognateArray = jQuery.extend(true, {}, data).results
                let cognateArray_test = jQuery.extend(true, {}, data).results
                $('#addOptionForm').hide(500)

                // add language families to groups (because latin doesn't use protection)
                // for each language group, work out if there are multiple children in the 
                // current dataset
                let languagesInDataset = cognateArray.map(e => e.lang)

                // here is where we slot the language groupings in
                cognateArray.forEach(applyGroups)
                function applyGroups (language) {
          for (let i = 0; i < languageGroups.length; i++) {
            // is the language a child of a group? and is the language's
            // parent out of that group?
            if (languageGroups[i].children.map(e => e.code).indexOf(language.lang) != -1 && languageGroups[i].children.map(e => e.code).indexOf(language.parentLang) == -1) {
              // if the group doesn't already exist, add the group to the array of words
              if (cognateArray.filter(e => e.lang == languageGroups[i].code && e.parentLang == language.parentLang && e.parentLatinTranscription == language.parentLatinTranscription).length == 0) {
                if (languageGroups[i].code != language.parentLang) {
                  cognateArray.push({ 'group': true, 'lang': languageGroups[i].code, 'languageName': languageGroups[i].name, 'latinTranscription': language.parentLatinTranscription, 'parentLatinTranscription': language.parentLatinTranscription, 'parentLang': language.parentLang, 'rootLatinTranscription': language.rootLatinTranscription })
                                };
              };
              // "shift" the language down
              language.parentLang = languageGroups[i].code
                        };
          };
        };

        let i = 0

                // create a tree from a flat array
                function createTree (inputArray, childField, parentJoinFields, childJoinFields, rootFields) {
          // find your root element
          function findRoot (element) {
            let numberToMatch = rootFields.length
                        let matched = 0
                        for (let i = 0; i < numberToMatch; i++) {
              if (element[childJoinFields[i]] == rootFields[i]) { matched++};
            };
            return numberToMatch == matched
                    };
          let returnObj = inputArray.filter(findRoot)

                    if (i < 1000) {
            i++

                        function findChildren (element) {
              let numberToMatch = parentJoinFields.length
                            let matched = 0
                            for (let i = 0; i < numberToMatch; i++) {
                if (element[parentJoinFields[i]] == rootFields[i]) { matched++};
              };
              return numberToMatch == matched
                        };

            // find array of children
            let childArray = inputArray.filter(findChildren)

                        // for each child, make its children the elements that have
                        // its ID as their parentID
                        for (let i = 0; i < childArray.length; i++) {
              let arrayToMatch = []
                            for (let j = 0; j < childJoinFields.length; j++) {
                arrayToMatch.push(childArray[i][childJoinFields[j]])
                            };
              childArray[i][childField] = createTree(inputArray, childField, parentJoinFields, childJoinFields, arrayToMatch)
                        };

            return (childArray)
                    }
        };

        // start the tree, currently only with the root element
        let newTree = cognateArray.filter(e => e.lang == rootLang && e.latinTranscription == rootLatinTranscription)[0]

                // expand the tree
                newTree['children'] = createTree(cognateArray, 'children', ['parentLang', 'parentLatinTranscription'], ['lang', 'latinTranscription'], [rootLang, rootLatinTranscription])

                // remove the latinTranscription of any nodes with group=true
                // (latinTranscription is needed to construct the tree, but logically, groups have no latinTranscription)
                function fixGroup (childArray) {
          let returnVar = childArray
                    for (let i = 0; i < childArray.length; i++) {
            if (childArray[i].children) {
              returnVar[i].children = fixGroup(returnVar[i].children)
                        }
            if (childArray[i].group) {
              returnVar[i].latinTranscription = null
                        };
          };
          return (returnVar)
                }
        newTree.children = fixGroup(newTree.children)

                // trim the data (remove nodes without a cognate)
                newTree = trimJSONObject(newTree, 'latinTranscription', true, true)

        // track the numnber of nodes in our tree
        var nodesInTree = new Array()
                // how many words are we missing?
                async function getMissingNodes () {
          nodesInTree.push({ 'lang': newTree.lang, 'latinTranscription': newTree.latinTranscription })
                    newTree.children.forEach(enumerateNodes)
          function enumerateNodes (d) {
            if (!d.group) {
              nodesInTree.push({ 'lang': d.lang, 'latinTranscription': d.latinTranscription, 'parentLang': d.parentLang, 'parentLatinTranscription': d.parentLatinTranscription })
                        };
            if (d.children) {
              d.children.forEach(enumerateNodes)
            }
          }
          let missingWords = data.results.filter(e1 => (
            nodesInTree.filter(e2 => (e2.lang == e1.lang && e2.latinTranscription == e1.latinTranscription)).length == 0
          )
          )
                    // keep this console.log, as it's used to see if there are any more words
                    // we can add to the tree
                    console.log({ 'Missing Words': missingWords })
                };
        getMissingNodes()							

                // remove any nodes past given depth (decided by user)
                var treeDepth = Number($('#depthSlider').val())
                function limitDepth (tree, currentDepth) {
          if (tree.children) {
            if (currentDepth >= treeDepth) {
              delete tree.children
                        } else {
              for (let i = 0; i < tree.children.length; i++) {
                tree.children[i] = limitDepth(tree.children[i], currentDepth + 1)
              };
            }
          };
          return tree
                };
        newTree = limitDepth(newTree, 0)

                // lower the treeDepth if the tree is actually less deep than the 
                // value supplied by the user (so we use the full screen width)
                treeDepth = Math.min(getJSONDepth(newTree, 'children'), treeDepth)
                var treeMap = d3.tree().size([svgHeight - 20, svgWidth])
                // Assigns parent, children, height, depth
                root = d3.hierarchy(newTree, function (d) { return d.children })
                
                // this is the tree we plot
                root.x0 = svgHeight / 2
                root.y0 = 0

                // Collapse after the second level
                root.children.forEach(collapse)

                update(root)

                // Collapse the node and all its children
                function collapse (d) {
          if (!d.group) {
          };

          if (d.children) {
            d._children = d.children
            d._children.forEach(collapse)
            d.children = null
          }
        };

        function update (source) {
          // Assigns the x and y position for the nodes
          var treeData = treeMap(root)

                    // Compute the new tree layout.
                    var nodes = treeData.descendants();
                        var links = treeData.descendants().slice(1)

                    // Normalize for fixed-depth.
                    nodes.forEach(function (d) { d.y = d.depth * (window.innerWidth / (treeDepth + 1)) })

                    // ****************** Nodes section ***************************

                    // Update the nodes...
                    var node = svg.selectAll('g.node')
            .data(nodes, function (d) { return d.id || (d.id = ++i) })

                    // Enter any new modes at the parent's previous position.
                    var nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr('data-lang', function (d) { return d.data.lang})
            .attr('data-url', function (d) { return d.data.url})
            .attr('transform', function (d) {
              return 'translate(' + source.y0 + ',' + source.x0 + ')';
            })
            .on('click', click)

                    // Add Circle for the nodes
                    nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', function (d) {
              return d._children ? '6px' : '4px';
            })
            .style('fill', function (d) {
              if (d.data.isBorrowed) {
                return borrowedColour
                                }
              if (d.data.soundChanges) {
                for (let i = 0; i < d.data.soundChanges.length; i++) {
                  if (d.data.soundChanges[i].name == selectedSoundChange) {
                    return soundChangeColour
                                        }
                }
              }
              return primaryColour
                            })
            .style('stroke', function (d) {
              // if (d.data.isBorrowed) {
              // 	return borrowedColour
              // } else {;
              return d._children ? greyOutlineColour : primaryColour
                                    // };
                            })
            .style('stroke-width', 1)
                            

                    // Add language names to the nodes
                    nodeEnter.append('text')
            .text(function (d) { return d.data.lang == rootLang ? '' : d.data.languageName })
            .attr('class', 'label')
            .attr('dy', '.35em')
            .attr('y', function (d) {
              let returVar = d.data.lang == rootLang ? 13 : -13
                            return (returVar)
                        })
            .attr('text-anchor', function (d) {
              let returVar = d.data.lang == rootLang ? 'start' : 'middle';
              return (returVar)
                        })
            .attr('textLength', function () {
              let textLength = d3.select(this).node().getComputedTextLength()
                            if (textLength > window.innerWidth / (treeDepth - 2) - 20) {
                return window.innerWidth / (treeDepth - 2) - 20
                            }
            })
            .attr('lengthAdjust', 'spacingAndGlyphs')
                        

                    // add the latin/ipa transcriptions of the word
                    var wordLabels = nodeEnter.append('text')
            .text(function (d) {
              if (d.data.group) {
                return (null)
                            } else {
 ;
                return ($('#ipaToggleContainer input').prop('checked') && d.data.ipaTranscription) ? d.data.ipaTranscription : d.data.latinTranscription
                            };
            })
            .style('font-size', '2vh')
            .attr('dy', '.35em')
            .attr('x', function (d) { return d.children || d._children ? 13 : 13})
            .attr('text-anchor', 'start')
            .attr('textLength', function () {
              let textLength = d3.select(this).node().getComputedTextLength()
                            if (textLength > window.innerWidth / (treeDepth + 1) - 20) {
                return window.innerWidth / (treeDepth + 1) - 20
                            }
            })
            .attr('lengthAdjust', 'spacingAndGlyphs')
                        
                            
                        // UPDATE
                        var nodeUpdate = nodeEnter.merge(node)

                        // Transition to the proper position for the node
                        nodeUpdate.transition()
            .duration(duration)
            .attr('transform', function (d) {
              return 'translate(' + d.y + ',' + d.x + ')';
            })

                        // Update the node attributes and style
                        nodeUpdate.select('circle.node')
            .attr('r', function (d) {
              return d._children ? '6px' : '4px';
            })
            .style('fill', primaryColour)
            .style('stroke', function (d) {
              return d._children ? greyOutlineColour : primaryColour
                            })
            .style('stroke-width', 2)
            .attr('cursor', 'pointer')
                            

                        // Remove any exiting nodes
                        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr('transform', function (d) {
              return 'translate(' + source.y + ',' + source.x + ')';
            })
            .remove()

                        // On exit reduce the node circles size to 0
                        nodeExit.select('circle')
            .attr('r', 1e-6)

                        // On exit reduce the opacity of text labels
                        nodeExit.select('text')
            .style('fill-opacity', 1e-6)

                        // ****************** links section ***************************

                        // Update the links...
                        var link = svg.selectAll('path.link')
            .data(links, function (d) { return d.id })

                        // Enter any new links at the parent's previous position.
                        var linkEnter = link.enter().insert('path', 'g')
            .attr('class', 'link')
            .attr('id', function (d) {
              return (d.data.lang + '-link')
                                })
            .attr('d', function (d) {
              var o = { x: source.x0, y: source.y0 }
              return diagonal(o, o)
            })
            .style('stroke', function (d) {
              if (d.data.soundChanges) {
                for (let i = 0; i < d.data.soundChanges.length; i++) {
                  if (d.data.soundChanges[i].name == selectedSoundChange) {
                    return soundChangeColour
                                            }
                }
              }

              if (d.data.isBorrowed) {
                return (borrowedColour)
              } else if (d.data.isDerived || d.data.lang == d.data.parentLang || d.data.lang == rootLang) {
                return (primaryColour)
              } else {
                return (directDescendantColour)
                                    }
            })
                                


                        // UPDATE
                        var linkUpdate = linkEnter.merge(link)

                        // Transition back to the parent element position
                        linkUpdate.transition()
            .duration(duration)
            .attr('d', function (d) { return diagonal(d, d.parent) })

                        // Remove any exiting links
                        var linkExit = link.exit().transition()
            .duration(duration)
            .attr('d', function (d) {
              var o = { x: source.x, y: source.y }
              return diagonal(o, o)
            })
            .remove()

                        // Store the old positions for transition.
                        nodes.forEach(function (d) {
            d.x0 = d.x
                            d.y0 = d.y
                        })

                        // Creates a curved (diagonal) path from parent to the child nodes
                        function diagonal (s, d) {
            path = `M ${d.y} ${d.x}
                                            C ${(s.y + d.y) / 2} ${d.x},
                                                ${(s.y + d.y) / 2} ${s.x},
                                                ${s.y} ${s.x}`

            return path
          }
          // here we decide what happens when you click a node.
          // default is to expand, but other admin options are available
          function click (d) {
            switch (currentMode) {
              case 'add':
                originalWord = null
                                    var blankWord = { 'parentLang': d.data.lang, 'parentLatinTranscription': d.data.latinTranscription, 'meaning': d.data.meaning, 'tags': [], 'soundChanges': [] }
                                    blankWord.rootLang = d.data.rootLang
                                    blankWord.rootLatinTranscription = d.data.rootLatinTranscription
                                    
                                    $('#af-languageName').prop('disabled', false)
                                    populateAdminForm(blankWord)
                                    toggleAdminForm()
                                    currentMode = 'default';
                $('#selectMode').val('default')
                break
                                
                                case 'edit':
                originalWord = d.data
                                    populateAdminForm(d.data)
                                    $('#af-languageName').prop('disabled', true)
                                    toggleAdminForm()
                                    currentMode = 'default';
                $('#selectMode').val('default')
                break

                                case 'delete':
                var alertMessage = 'Deleting word ';
                alertMessage += d.data.latinTranscription
                                    alertMessage += ' for language ' + d.data.languageName
                                    alertMessage += '. Sure you want to do this?';
                alertMessage += ' This is irreversible so please be careful!'

                if (confirm(alertMessage)) {
                  deleteWord(d.data)
                                    };
                currentMode = 'default'
                $('i[data-active]').attr('data-active', 0)
                                    currentMode = 'default';
                $('#selectMode').val('default')
                break

                                case 'changeParent':
                originalWord = jQuery.extend(true, {}, d.data)
                                    childToMove = d.data

                                    currentMode = 'changeParent_2'
                var alertMessage = 'Changing parent for the ';
                alertMessage += d.data.languageName + ' word ';
                alertMessage += d.data.latinTranscription
                                    alertMessage += '. Click the node you want to be the parent,';
                alertMessage += " or reload if you don't want to do this."
                                    alert(alertMessage)
                break

                                case 'changeParent_2':
                var oldParent = allLanguages.filter(e => (e.code == originalWord.parentLang || e.code + '-Latn' == originalWord.parentLang))[0]
                                    var oldParentName = oldParent.name
                                    var oldParentCode = oldParent.code
                                    childToMove.parentLang = d.data.lang
                                    childToMove.parentLatinTranscription = d.data.latinTranscription
                                    currentMode = 'changeParent';
                populateAdminForm(childToMove)
                                    var alertMessage = 'Changing parent for ';
                alertMessage += d.data.latinTranscription
                                    alertMessage += ' from ' + oldParentName
                                    alertMessage += ' to ' + d.data.languageName
                                    alertMessage += '. Sure you want to do this?'
                if (oldParentCode.slice(0, 6) == 'group-') {
                  alert("Can't move parent to a group.")
                                    } else if (confirm(alertMessage)) {
                  submitWordChanges()
                                    };
                $('i[data-active]').attr('data-active', 0)
                                    currentMode = 'default'
                $('#selectMode').val('default')
                break

                                case 'wiki':
                var wordURL = 'https://en.wiktionary.org' + d.data.url
                window.open(wordURL)
                                    break;

              default:
                if (d.children) {
                  d._children = d.children
                                        d.children = null
                                    } else {
                  d.children = d._children
                                        d._children = null
                                    }
                update(d)
                                    break;
            };
          };

          // populate the admin form when editing or adding a node
          function populateAdminForm (d) {
            $('#af-languageName').val(d.languageName)

                            $('#af-isBorrowed input').prop('checked', d.isBorrowed)

                            $('#af-languageCode').val(d.lang)

                            $('#af-rootLatinTranscription').val(d.rootLatinTranscription)
                            $('#af-rootLanguageCode').val(rootLang)
                            $('#af-latinTranscription').val(d.latinTranscription)
                            $('#af-ipaTranscription').val(d.ipaTranscription)
                            $('#af-meaning').val(d.meaning)
                            
                            // we don't really want things being edited for the root word
                            if (d.latinTranscription == d.rootLatinTranscription) {
              $('#soundChangeli').hide()
                                $('#tagli').show()
                                $('#af-parentLanguageName').val('')
                                $('#af-parentLanguageCode').val('')
                                $('#af-parentLanguageName').prop('disabled', true)
              $('#af-latinTranscription').prop('disabled', true)
              $('#af-parentLatinTranscription').val('')
              $('#af-parentLatinTranscription').prop('disabled', true)
            } else {
              $('#soundChangeli').show()
                                $('#tagli').hide()

                                var parentLanguageName = d.lang == rootLang ? d.parentLang : allLanguages.filter(e => (e.code == d.parentLang || e.code + '-Latn' == d.parentLang))[0].name
                                $('#af-parentLanguageName').val(parentLanguageName)
                                $('#af-parentLanguageCode').val(d.parentLang)
                                $('#af-parentLanguageName').prop('disabled', false)
              $('#af-latinTranscription').prop('disabled', false)
              $('#af-parentLatinTranscription').val(d.parentLatinTranscription)
              $('#af-parentLatinTranscription').prop('disabled', false)

              // let's deal with sound changes specific to certain languages
              // proto-germanic only (grimm, verner)
              $('#soundChange-list li').show()
                                if (d.lang != 'gem-pro') {
                $('#soundChange-list li[data-value=grimm]').hide() 
                                    $('#soundChange-list li[data-value=kluge]').hide() 
                                    $('#soundChange-list li[data-value=verner]').hide() 
                                };
              var grassmanLangs = ['sa-Latn', 'grk-pro', 'grc-Latn', 'el-Latn', 'inc-pro', 'ira-pro', 'iir-pro']
              if (grassmanLangs.indexOf(d.lang) == -1) {
                $('#soundChange-list li[data-value=grassman]').hide() 
                                };
              if (d.lang != 'es' && d.lang != 'osp') {
                $('#soundChange-list li[data-value=La_al-Sp_o_t]').hide() 
                                };
              if (d.lang != 'cel-pro') {
                $('#soundChange-list li[data-value=PIE_p-CEL_0]').hide() 
                                };
            };

            // remove all selected classes
            $('#tag-dropdown li').removeClass('selected')
                            $('#soundChange-list li').removeClass('selected')

                            for (let i = 0; i < d.soundChanges.length; i++) {
              if (d.soundChanges[i].val) { $('#soundChange-list li[data-value=' + d.soundChanges[i].name + ']').addClass('selected') };
            };
            for (let i = 0; i < d.tags.length; i++) {
              if (d.tags[i].val) { $('#tag-dropdown li[data-value=' + d.tags[i].name + ']').addClass('selected') };
            };
          };
        };
        // now add the root word in English
        d3.select('#svgCanvas svg')
          .append('text')
          .classed('label', true)
          .style('stroke', primaryColour)
          .style('fill', primaryColour)
          .style('stroke-width', 0.5)
          .attr('y', 28)
          .attr('text-anchor', 'left')
          .attr('class', 'graph-headword')
          .attr('font-weight', 'lighter')
          .text(function (d) {
            var rootLanguage = cognateArray.filter(e => e.lang == rootLang && e.latinTranscription == rootLatinTranscription)[0]
                                return (rootLanguage.languageName + ': ' + rootLanguage.latinTranscription + ' (' + rootLanguage.meaning + ')')
                            })
                            

        } }) 
};

function toggleSettingsForm () {
  let currentOpacity = $('#svgCanvas').css('opacity')
    let newOpacity = 1.5 - currentOpacity
    $('#svgCanvas').css('opacity', newOpacity)
    $('#settingsForm').toggle()
};

function toggleAdminMode () {
  $('#soundChangeli').hide()
    $('#tagli').hide()
    $('#selectMode').toggle()
    $('input[name=adminCode]').toggle()
    var isInAdminMode = $('#adminToggleContainer input').prop('checked')
    const oneMonthFromNow = moment().add(30, 'days').utc()._d
    document.cookie = adminCookieName + '=' + String(isInAdminMode) + '; expires=' + oneMonthFromNow
};

function toggleAdminForm () {
  let currentOpacity = $('#svgCanvas').css('opacity')
    let newOpacity = 1.5 - currentOpacity
    $('#svgCanvas').css('opacity', newOpacity)
    $('#adminForm').toggle()
};

var currentMode = 'default';
function toggleActive (el) {
  var isActive = $(el).attr('data-active') == 1
    if (isActive) {
    $(el).attr('data-active', 0)
        currentMode = 'default';
  } else {
    $('i[data-active=1]').attr('data-active', 0)
        $(el).attr('data-active', 1)
        currentMode = $('i[data-active=1]').attr('data-mode')
    };

  if (currentMode == 'add') {
    var alertMessage = "Click the node to which you'd like to add a descendant."
        alert(alertMessage)
    };
}

var csrf_token
// drawChart("*albʰós");
