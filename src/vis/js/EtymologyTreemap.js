var cookies = document.cookie.split(";").reduce((res, c) => {
  const [key, val] = c
    .trim()
    .split("=")
    .map(decodeURIComponent);
  const allNumbers = str => /^\d+$/.test(str);
  try {
    return Object.assign(res, {
      [key]: allNumbers(val) ? val : JSON.parse(val)
    });
  } catch (e) {
    return Object.assign(res, {
      [key]: val
    });
  }
}, {});

const duration = 750;

// get all languages, regardless of whether or not we have words for them
export async function getAllLanguages() {
  const result = await fetch('/django/indo-european-cognates-2/', {
    method: 'POST',
    headers: {
      'x-csrftoken': cookies.csrftoken
    },
    body: JSON.stringify({
      'requestType': 'getAllLanguages'
    }),
    credentials: 'include',
    mode: 'cors'
  })
  var allLangs = await result.json();
  return allLangs.results;
};

export async function getAllRootLangs() {
  const requestBody = {
    requestType: "getAllRootLangs"
  };
  const result = await fetch(
    "/django/indo-european-cognates-2/", {
      method: "POST",
      headers: {
        "x-csrftoken": cookies.csrftoken
      },
      body: JSON.stringify(requestBody),
      credentials: "include",
      mode: "cors"
    }
  );
  var rootLangs = await result.json();
  return rootLangs;
};

// get list of language groupings (to make it prettier)
export async function getLanguageGroups() {
  const result = await fetch('indoEuropeanGroups.json', {
    type: 'GET',
    credentials: 'include'
  })
  return result
};
var languageGroups = [];

export function createHierarchy(cognateArray, rootLang, rootLatinTranscription, languageGroups) {
  // here is where we slot the language groupings in
  cognateArray.forEach(applyGroups)

  function applyGroups(language) {
    for (let i = 0; i < languageGroups.length; i++) {
      // is the language a child of a group? and is the language's
      // parent out of that group?
      if (languageGroups[i].children.map(e => e.code).indexOf(language.lang) != -1 && languageGroups[i].children.map(e => e.code).indexOf(language.parentLang) == -1) {
        // if the group doesn't already exist, add the group to the array of words
        if (cognateArray.filter(e => e.lang == languageGroups[i].code && e.parentLang == language.parentLang && e.parentLatinTranscription == language.parentLatinTranscription).length == 0) {
          if (languageGroups[i].code != language.parentLang) {
            cognateArray.push({
              'group': true,
              'lang': languageGroups[i].code,
              'languageName': languageGroups[i].name,
              'latinTranscription': language.parentLatinTranscription,
              'parentLatinTranscription': language.parentLatinTranscription,
              'parentLang': language.parentLang,
              'rootLatinTranscription': language.rootLatinTranscription,
              'topLabel': languageGroups[i].name,
              'circleStyle': {
                fill: "var(--main-brand)"
              }

            })
          };
        };
        // "shift" the language down
        language.parentLang = languageGroups[i].code
      };
    };
  };

  var i = 0;

  // create a tree from a flat array
  function createTree(inputArray, childField, parentJoinFields, childJoinFields, rootFields) {
    // find your root element
    function findRoot(element) {
      let numberToMatch = rootFields.length
      let matched = 0
      for (let i = 0; i < numberToMatch; i++) {
        if (element[childJoinFields[i]] == rootFields[i]) {
          matched++
        };
      };
      return numberToMatch == matched
    };
    let returnObj = inputArray.filter(findRoot)

    if (i < 1000) {
      i++

      function findChildren(element) {
        let numberToMatch = parentJoinFields.length
        let matched = 0
        for (let i = 0; i < numberToMatch; i++) {
          if (element[parentJoinFields[i]] == rootFields[i]) {
            matched++
          };
        };
        return numberToMatch == matched
      };

      // find array of children
      let childArray = inputArray.filter(findChildren)

      // for each child, make its children the elements that have
      // its ID as their parentID
      for (let i = 0; i < childArray.length; i++) {
        let arrayToMatch = []
        for (let j = 0; j < childJoinFields.length; j++) {
          arrayToMatch.push(childArray[i][childJoinFields[j]])
        };
        childArray[i][childField] = createTree(inputArray, childField, parentJoinFields, childJoinFields, arrayToMatch)
      };

      return (childArray)
    }

    return newTree
  };
  // remove the latinTranscription of any nodes with group=true
  // (latinTranscription is needed to construct the tree, but logically, groups have no latinTranscription)
  function fixGroup(childArray) {
    let returnVar = childArray
    for (let i = 0; i < childArray.length; i++) {
      if (childArray[i].children) {
        returnVar[i].children = fixGroup(returnVar[i].children)
      }
      if (childArray[i].group) {
        returnVar[i].latinTranscription = null
      };
    };
    return (returnVar)
  }

  // start the tree, currently only with the root element
  let newTree = cognateArray.filter(e => e.lang == rootLang && e.latinTranscription == rootLatinTranscription)[0]
  // expand the tree
  newTree['children'] = createTree(cognateArray, 'children', ['parentLang', 'parentLatinTranscription'], ['lang', 'latinTranscription'], [rootLang, rootLatinTranscription])
  newTree.children = fixGroup(newTree.children)
  // trim the data (remove nodes without a cognate)
  newTree = trimJSONObject(newTree, 'latinTranscription', true, true)
  return newTree;
}

export function drawChartFromWords(data, allLanguages, languageGroups, rootLang, rootLatinTranscription, svgHeight, svgWidth) {
  var currentMode = "default";

  const margin = {
    left: 12,
    top: 20,
    right: 0,
    bottom: 40
  };
  svgHeight = svgHeight - margin.bottom - margin.top;
  var svg = d3.select("#svgCanvas").append("svg")
    .attr("width", svgWidth + margin.right + margin.left)
    .attr("height", svgHeight)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  // get the groupings

  for (let i = 0; i < allLanguages.length; i++) {
    for (let j = 0; j < data.results.length; j++) {
      if (allLanguages[i].code == data.results[j].lang || (allLanguages[i].code + '-Latn') == data.results[j].lang) {
        data.results[j].languageName = allLanguages[i].name
      }
    };
  };
  // we use jquery.extend to deep copy the object
  let cognateArray = data.results.map(e => e);

  // add language families to groups (because latin doesn't use protection)
  // for each language group, work out if there are multiple children in the 
  // current dataset
  let languagesInDataset = cognateArray.map(e => e.lang)

  // here is where we slot the language groupings in
  cognateArray.forEach(applyGroups)

  function applyGroups(language) {
    for (let i = 0; i < languageGroups.length; i++) {
      // is the language a child of a group? and is the language's
      // parent out of that group?
      if (languageGroups[i].children.map(e => e.code).indexOf(language.lang) != -1 && languageGroups[i].children.map(e => e.code).indexOf(language.parentLang) == -1) {
        // if the group doesn't already exist, add the group to the array of words
        if (cognateArray.filter(e => e.lang == languageGroups[i].code && e.parentLang == language.parentLang && e.parentLatinTranscription == language.parentLatinTranscription).length == 0) {
          if (languageGroups[i].code != language.parentLang) {
            cognateArray.push({
              'group': true,
              'lang': languageGroups[i].code,
              'languageName': languageGroups[i].name,
              'latinTranscription': language.parentLatinTranscription,
              'parentLatinTranscription': language.parentLatinTranscription,
              'parentLang': language.parentLang,
              'rootLatinTranscription': language.rootLatinTranscription
            })
          };
        };
        // "shift" the language down
        language.parentLang = languageGroups[i].code
      };
    };
  };

  let i = 0

  // create a tree from a flat array
  function createTree(inputArray, childField, parentJoinFields, childJoinFields, rootFields) {
    // find your root element
    function findRoot(element) {
      let numberToMatch = rootFields.length
      let matched = 0
      for (let i = 0; i < numberToMatch; i++) {
        if (element[childJoinFields[i]] == rootFields[i]) {
          matched++
        };
      };
      return numberToMatch == matched
    };
    let returnObj = inputArray.filter(findRoot)

    if (i < 1000) {
      i++

      function findChildren(element) {
        let numberToMatch = parentJoinFields.length
        let matched = 0
        for (let i = 0; i < numberToMatch; i++) {
          if (element[parentJoinFields[i]] == rootFields[i]) {
            matched++
          };
        };
        return numberToMatch == matched
      };

      // find array of children
      let childArray = inputArray.filter(findChildren)

      // for each child, make its children the elements that have
      // its ID as their parentID
      for (let i = 0; i < childArray.length; i++) {
        let arrayToMatch = []
        for (let j = 0; j < childJoinFields.length; j++) {
          arrayToMatch.push(childArray[i][childJoinFields[j]])
        };
        childArray[i][childField] = createTree(inputArray, childField, parentJoinFields, childJoinFields, arrayToMatch)
      };

      return (childArray)
    }
  };

  // start the tree, currently only with the root element
  let newTree = cognateArray.filter(e => e.lang == rootLang && e.latinTranscription == rootLatinTranscription)[0]

  // expand the tree
  newTree['children'] = createTree(cognateArray, 'children', ['parentLang', 'parentLatinTranscription'], ['lang', 'latinTranscription'], [rootLang, rootLatinTranscription])

  // remove the latinTranscription of any nodes with group=true
  // (latinTranscription is needed to construct the tree, but logically, groups have no latinTranscription)
  function fixGroup(childArray) {
    let returnVar = childArray
    for (let i = 0; i < childArray.length; i++) {
      if (childArray[i].children) {
        returnVar[i].children = fixGroup(returnVar[i].children)
      }
      if (childArray[i].group) {
        returnVar[i].latinTranscription = null
      };
    };
    return (returnVar)
  }
  newTree.children = fixGroup(newTree.children)

  // trim the data (remove nodes without a cognate)
  newTree = trimJSONObject(newTree, 'latinTranscription', true, true)

  // track the numnber of nodes in our tree
  var nodesInTree = new Array()
  // how many words are we missing?
  async function getMissingNodes() {
    nodesInTree.push({
      'lang': newTree.lang,
      'latinTranscription': newTree.latinTranscription
    })
    newTree.children.forEach(enumerateNodes)

    function enumerateNodes(d) {
      if (!d.group) {
        nodesInTree.push({
          'lang': d.lang,
          'latinTranscription': d.latinTranscription,
          'parentLang': d.parentLang,
          'parentLatinTranscription': d.parentLatinTranscription
        })
      };
      if (d.children) {
        d.children.forEach(enumerateNodes)
      }
    }
    let missingWords = data.results.filter(e1 => (
      nodesInTree.filter(e2 => (e2.lang == e1.lang && e2.latinTranscription == e1.latinTranscription)).length == 0
    ))
    // keep this console.log, as it's used to see if there are any more words
    // we can add to the tree
    console.log({
      'Missing Words': missingWords
    })
  };
  getMissingNodes()

  // remove any nodes past given depth (decided by user)
  var treeDepth = 6;

  function limitDepth(tree, currentDepth) {
    if (tree.children) {
      if (currentDepth >= treeDepth) {
        delete tree.children
      } else {
        for (let i = 0; i < tree.children.length; i++) {
          tree.children[i] = limitDepth(tree.children[i], currentDepth + 1)
        };
      }
    };
    return tree
  };
  newTree = limitDepth(newTree, 0)

  // lower the treeDepth if the tree is actually less deep than the 
  // value supplied by the user (so we use the full screen width)
  treeDepth = Math.min(getJSONDepth(newTree, 'children'), treeDepth)
  var treeMap = d3.tree().size([svgHeight - 20, svgWidth])
  // Assigns parent, children, height, depth
  var root = d3.hierarchy(newTree, function (d) {
    return d.children
  })

  // this is the tree we plot
  root.x0 = svgHeight / 2
  root.y0 = 0

  // Collapse after the second level
  root.children.forEach(collapse)

  update(root)

  // Collapse the node and all its children
  function collapse(d) {
    if (!d.group) {};

    if (d.children) {
      d._children = d.children
      d._children.forEach(collapse)
      d.children = null
    }
  };

  function update(source) {
    // Assigns the x and y position for the nodes
    var treeData = treeMap(root)

    // Compute the new tree layout.
    var nodes = treeData.descendants();
    var links = treeData.descendants().slice(1)

    // Normalize for fixed-depth.
    nodes.forEach(function (d) {
      d.y = d.depth * (window.innerWidth / (treeDepth + 1))
    });

    // ****************** Nodes section ***************************

    // Update the nodes...
    var node = svg.selectAll('g.node')
      .data(nodes, function (d) {
        return d.id || (d.id = ++i)
      })

    // Enter any new modes at the parent's previous position.
    var nodeEnter = node.enter().append('g')
      .attr('class', 'node')
      .attr('data-lang', function (d) {
        return d.data.lang
      })
      .attr('data-url', function (d) {
        return d.data.url
      })
      .attr('transform', function (d) {
        return 'translate(' + source.y0 + ',' + source.x0 + ')';
      })
      .on('click', click)

    // Add Circle for the nodes
    nodeEnter.append('circle')
      .attr('class', 'node')
      .attr("data-children", function (d) {
        return d._children ? true : false;
      })


    // Add language names to the nodes
    nodeEnter.append('text')
      .text(function (d) {
        return d.data.lang == rootLang ? '' : d.data.languageName
      })
      .attr('class', 'label')
      .attr('dy', '.35em')
      .attr('y', function (d) {
        let returVar = d.data.lang == rootLang ? 13 : -13
        return (returVar)
      })
      .attr('text-anchor', function (d) {
        let returVar = d.data.lang == rootLang ? 'start' : 'middle';
        return (returVar)
      })
      .attr('textLength', function () {
        let textLength = d3.select(this).node().getComputedTextLength()
        if (textLength > window.innerWidth / (treeDepth - 2) - 20) {
          return window.innerWidth / (treeDepth - 2) - 20
        }
      })
      .attr('lengthAdjust', 'spacingAndGlyphs')


    // add the latin/ipa transcriptions of the word
    var wordLabels = nodeEnter.append('text')
      .text(function (d) {
        if (d.data.group) {
          return (null)
        } else {
          ;
          return d.data.latinTranscription
        };
      })
      .classed("cognate-label", true)
      .style('font-size', '2vh')
      .attr('dy', '.35em')
      .attr('x', function (d) {
        return d.children || d._children ? 13 : 13
      })
      .attr('text-anchor', 'start')
      .attr('textLength', function () {
        let textLength = d3.select(this).node().getComputedTextLength()
        if (textLength > window.innerWidth / (treeDepth + 1) - 20) {
          return window.innerWidth / (treeDepth + 1) - 20
        }
      })
      .attr('lengthAdjust', 'spacingAndGlyphs')


    // UPDATE
    var nodeUpdate = nodeEnter.merge(node)

    // Transition to the proper position for the node
    nodeUpdate.transition()
      .duration(duration)
      .attr('transform', function (d) {
        return 'translate(' + d.y + ',' + d.x + ')';
      })

    // Update the node attributes and style
    nodeUpdate.select('circle.node')
      .attr("data-children", function (d) {
        return d._children ? true : false;
      })
      .attr('cursor', 'pointer')


    // Remove any exiting nodes
    var nodeExit = node.exit().transition()
      .duration(duration)
      .attr('transform', function (d) {
        return 'translate(' + source.y + ',' + source.x + ')';
      })
      .remove()

    // On exit reduce the node circles size to 0
    nodeExit.select('circle')
      .attr('r', 1e-6)

    // On exit reduce the opacity of text labels
    nodeExit.select('text')
      .style('fill-opacity', 1e-6)

    // ****************** links section ***************************

    // Update the links...
    var link = svg.selectAll('path.link')
      .data(links, function (d) {
        return d.id
      })

    // Enter any new links at the parent's previous position.
    var linkEnter = link.enter().insert('path', 'g')
      .attr('class', 'link')
      .attr('id', function (d) {
        return (d.data.lang + '-link')
      })
      .attr('d', function (d) {
        var o = {
          x: source.x0,
          y: source.y0
        }
        return diagonal(o, o)
      })
      .attr("data-derived", function (d) {
        if (d.data.isDerived || d.data.lang == d.data.parentLang || d.data.lang == rootLang) {
          return true
        }
      })
      .attr("data-borrowed", function (d) {
        if (d.data.isBorrowed) {
          return true
        }
      });



    // UPDATE
    var linkUpdate = linkEnter.merge(link)

    // Transition back to the parent element position
    linkUpdate.transition()
      .duration(duration)
      .attr('d', function (d) {
        return diagonal(d, d.parent)
      })

    // Remove any exiting links
    var linkExit = link.exit().transition()
      .duration(duration)
      .attr('d', function (d) {
        var o = {
          x: source.x,
          y: source.y
        }
        return diagonal(o, o)
      })
      .remove()

    // Store the old positions for transition.
    nodes.forEach(function (d) {
      d.x0 = d.x
      d.y0 = d.y
    })

    // Creates a curved (diagonal) path from parent to the child nodes
    function diagonal(s, d) {
      var path = `M ${d.y} ${d.x}
                  C ${(s.y + d.y) / 2} ${d.x},
                      ${(s.y + d.y) / 2} ${s.x},
                      ${s.y} ${s.x}`

      return path
    }
    // here we decide what happens when you click a node.
    // default is to expand, but other admin options are available
    function click(d) {
      switch (currentMode) {
        case 'add':
          originalWord = null
          var blankWord = {
            'parentLang': d.data.lang,
            'parentLatinTranscription': d.data.latinTranscription,
            'meaning': d.data.meaning,
            'tags': [],
            'soundChanges': []
          }
          blankWord.rootLang = d.data.rootLang
          blankWord.rootLatinTranscription = d.data.rootLatinTranscription

          $('#af-languageName').prop('disabled', false)
          populateAdminForm(blankWord)
          toggleAdminForm()
          currentMode = 'default';
          $('#selectMode').val('default')
          break

        case 'edit':
          originalWord = d.data
          populateAdminForm(d.data)
          $('#af-languageName').prop('disabled', true)
          toggleAdminForm()
          currentMode = 'default';
          $('#selectMode').val('default')
          break

        case 'delete':
          var alertMessage = 'Deleting word ';
          alertMessage += d.data.latinTranscription
          alertMessage += ' for language ' + d.data.languageName
          alertMessage += '. Sure you want to do this?';
          alertMessage += ' This is irreversible so please be careful!'

          if (confirm(alertMessage)) {
            deleteWord(d.data)
          };
          currentMode = 'default'
          $('i[data-active]').attr('data-active', 0)
          currentMode = 'default';
          $('#selectMode').val('default')
          break

        case 'changeParent':
          originalWord = jQuery.extend(true, {}, d.data)
          childToMove = d.data

          currentMode = 'changeParent_2'
          var alertMessage = 'Changing parent for the ';
          alertMessage += d.data.languageName + ' word ';
          alertMessage += d.data.latinTranscription
          alertMessage += '. Click the node you want to be the parent,';
          alertMessage += " or reload if you don't want to do this."
          alert(alertMessage)
          break

        case 'changeParent_2':
          var oldParent = allLanguages.filter(e => (e.code == originalWord.parentLang || e.code + '-Latn' == originalWord.parentLang))[0]
          var oldParentName = oldParent.name
          var oldParentCode = oldParent.code
          childToMove.parentLang = d.data.lang
          childToMove.parentLatinTranscription = d.data.latinTranscription
          currentMode = 'changeParent';
          populateAdminForm(childToMove)
          var alertMessage = 'Changing parent for ';
          alertMessage += d.data.latinTranscription
          alertMessage += ' from ' + oldParentName
          alertMessage += ' to ' + d.data.languageName
          alertMessage += '. Sure you want to do this?'
          if (oldParentCode.slice(0, 6) == 'group-') {
            alert("Can't move parent to a group.")
          } else if (confirm(alertMessage)) {
            submitWordChanges()
          };
          $('i[data-active]').attr('data-active', 0)
          currentMode = 'default'
          $('#selectMode').val('default')
          break

        case 'wiki':
          var wordURL = 'https://en.wiktionary.org' + d.data.url
          window.open(wordURL)
          break;

        default:
          if (d.children) {
            d._children = d.children
            d.children = null
          } else {
            d.children = d._children
            d._children = null
          }
          update(d)
          break;
      };
    };

    // populate the admin form when editing or adding a node
    function populateAdminForm(d) {
      $('#af-languageName').val(d.languageName)

      $('#af-isBorrowed input').prop('checked', d.isBorrowed)

      $('#af-languageCode').val(d.lang)

      $('#af-rootLatinTranscription').val(d.rootLatinTranscription)
      $('#af-rootLanguageCode').val(rootLang)
      $('#af-latinTranscription').val(d.latinTranscription)
      $('#af-ipaTranscription').val(d.ipaTranscription)
      $('#af-meaning').val(d.meaning)

      // we don't really want things being edited for the root word
      if (d.latinTranscription == d.rootLatinTranscription) {
        $('#soundChangeli').hide()
        $('#tagli').show()
        $('#af-parentLanguageName').val('')
        $('#af-parentLanguageCode').val('')
        $('#af-parentLanguageName').prop('disabled', true)
        $('#af-latinTranscription').prop('disabled', true)
        $('#af-parentLatinTranscription').val('')
        $('#af-parentLatinTranscription').prop('disabled', true)
      } else {
        $('#soundChangeli').show()
        $('#tagli').hide()

        var parentLanguageName = d.lang == rootLang ? d.parentLang : allLanguages.filter(e => (e.code == d.parentLang || e.code + '-Latn' == d.parentLang))[0].name
        $('#af-parentLanguageName').val(parentLanguageName)
        $('#af-parentLanguageCode').val(d.parentLang)
        $('#af-parentLanguageName').prop('disabled', false)
        $('#af-latinTranscription').prop('disabled', false)
        $('#af-parentLatinTranscription').val(d.parentLatinTranscription)
        $('#af-parentLatinTranscription').prop('disabled', false)

        // let's deal with sound changes specific to certain languages
        // proto-germanic only (grimm, verner)
        $('#soundChange-list li').show()
        if (d.lang != 'gem-pro') {
          $('#soundChange-list li[data-value=grimm]').hide()
          $('#soundChange-list li[data-value=kluge]').hide()
          $('#soundChange-list li[data-value=verner]').hide()
        };
        var grassmanLangs = ['sa-Latn', 'grk-pro', 'grc-Latn', 'el-Latn', 'inc-pro', 'ira-pro', 'iir-pro']
        if (grassmanLangs.indexOf(d.lang) == -1) {
          $('#soundChange-list li[data-value=grassman]').hide()
        };
        if (d.lang != 'es' && d.lang != 'osp') {
          $('#soundChange-list li[data-value=La_al-Sp_o_t]').hide()
        };
        if (d.lang != 'cel-pro') {
          $('#soundChange-list li[data-value=PIE_p-CEL_0]').hide()
        };
      };

      // remove all selected classes
      $('#tag-dropdown li').removeClass('selected')
      $('#soundChange-list li').removeClass('selected')

      for (let i = 0; i < d.soundChanges.length; i++) {
        if (d.soundChanges[i].val) {
          $('#soundChange-list li[data-value=' + d.soundChanges[i].name + ']').addClass('selected')
        };
      };
      for (let i = 0; i < d.tags.length; i++) {
        if (d.tags[i].val) {
          $('#tag-dropdown li[data-value=' + d.tags[i].name + ']').addClass('selected')
        };
      };
    };
  };
  // now add the root word in English
  d3.select('#svgCanvas svg')
    .append('text')
    .classed('label', true)
    .attr('y', 28)
    .attr('text-anchor', 'left')
    .attr('class', 'graph-headword')
    .attr('font-weight', 'lighter')
    .text(function (d) {
      var rootLanguage = cognateArray.filter(e => e.lang == rootLang && e.latinTranscription == rootLatinTranscription)[0]
      return (rootLanguage.languageName + ': ' + rootLanguage.latinTranscription + ' (' + rootLanguage.meaning + ')')
    })


}
