import Vue from 'vue/dist/vue.js'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueLodash from 'vue-lodash'
import App from './App.vue'
import About from './About.vue'
import DataVisualisations from './DataVisualisations.vue'
import Etymology from './Etymology.vue'
import EtymologyTreemap from './vis/EtymologyTreemap.vue'
import EtymologyWeb from './vis/EtymologyWeb.vue'
import UKTrainDelay from './vis/UKTrainDelay.vue'
import UsedCarPrices from './vis/UsedCarPrices.vue'
import UKBabyNames from './vis/UKBabyNames.vue'
import LastFMBars from './vis/LastFMBars.vue'
import FPLValue from './vis/FPLValue.vue'
import Doublets from './vis/Doublets.vue'
import UKClimate from './vis/UKClimate.vue'
import LanguageLinks from './vis/LanguageLinks.vue'
import StarTrekScripts from './vis/StarTrekScripts.vue'
import ScriptParser from './vis/ScriptParser.vue'
import CarHunter from './apps/CarHunter.vue'
import Apps from './Apps.vue'
import BlogMenu from './BlogMenu.vue'
import BlogPost from './BlogPost.vue'
import {
  library
} from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret,
  faBars,
  faInfo,
  faChevronUp,
  faChevronDown,
  faWindowClose,
  faCog,
  faChartArea,
  faChartBar,
  faChartLine,
  faHatWizard,
  faGlobe,
  faGrin,
  faBlog

} from '@fortawesome/free-solid-svg-icons'
import {
  MdButton,
  MdContent,
  MdTabs
} from 'vue-material/dist/components'
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

Vue.use(VueRouter)
Vue.use(Vuex)
// Vue.use(VueLodash, {
//   name: 'lodash'
// })

library.add(faUserSecret, faBars, faInfo, faChevronUp, faChevronDown, faCog, faWindowClose, faChartArea,
  faChartBar, faChartLine, faHatWizard,
  faGlobe,
  faGrin, faBlog
)
Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)
Vue.use(require('vue-moment'));

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

// initiate vuex store for /vis/ page components
const store = new Vuex.Store({
  state: {
    ShowInfo: false,
    ShowControl: true,
    ShowSettings: false,
    FirstLoad: true
  },
  mutations: {
    toggleInfo(state) {
      state.ShowInfo = !state.ShowInfo;
    },
    toggleControl(state) {
      state.ShowControl = !state.ShowControl;
    },
    toggleSettings(state) {
      state.ShowSettings = !state.ShowSettings;
    },
    toggleFirstLoad(state) {
      state.FirstLoad = !state.FirstLoad;
    },
  }
});

// 1. Define route components.
// These can be imported from other files
// const Home = { template: '<a>Home</a>'}
// const DataVisualisations = { template: '<a>Data Visualisations</a>'}
// const Blog = { template: '<a>Blog</a>'}

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [{
    path: '/',
    component: App
  },
  {
    path: '/about',
    component: About
  },
  {
    path: '/data-visualisations/vis/etymology-treemap',
    component: EtymologyTreemap
  },
  {
    path: '/data-visualisations/vis/used-car-prices',
    component: UsedCarPrices
  },
  {
    path: '/apps/app/uk-train-delay',
    component: UKTrainDelay
  },
  {
    path: '/data-visualisations/vis/uk-baby-names',
    component: UKBabyNames
  },
  {
    path: '/data-visualisations/vis/last-fm-bars',
    component: LastFMBars
  },
  {
    path: '/data-visualisations/vis/fpl-value',
    component: FPLValue
  },
  {
    path: '/data-visualisations/vis/doublets',
    component: Doublets
  },
  {
    path: '/data-visualisations/vis/language-links',
    component: LanguageLinks
  },
  {
    path: '/data-visualisations/vis/uk-climate',
    component: UKClimate
  },
  {
    path: '/data-visualisations/vis/trek-scripts',
    component: StarTrekScripts
  },
  {
    path: '/data-visualisations/vis/script-parser',
    component: ScriptParser
  },
  {
    path: '/data-visualisations/vis/etymology-web',
    component: EtymologyWeb
  },
  {
    path: '/data-visualisations',
    component: DataVisualisations
  }, {
    path: '/etymology',
    component: Etymology
  }, {
    path: '/blog/post/:slug',
    component: BlogPost
  }, {
    path: '/blog',
    component: BlogMenu
  }, {
    path: '/apps',
    component: Apps
  }, {
    path: '/apps/app/car-hunter',
    component: CarHunter
  }

]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes, // short for `routes: routes`
  mode: 'history'
})

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
new Vue({
  el: "#app",
  router,
  store,
  components: {
    App
  }
})


// new Vue({
//   el: '#app',
//   components: { App },
//   template: '<App/>'
// });
