export default function debounce(func, wait, immediate) {
  // --------------------------------------------------------------------------
  // credit goes to https://davidwalsh.name/javascript-debounce-function
  // --------------------------------------------------------------------------
  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  console.log("debounce");
  console.log(func);
  console.log(wait);
  var timeout;
  return function () {
    console.log("here?");
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
